"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pizzaList = {
    gyros: [
        'Gyros Herbs',
        'Cheese',
        'Chicken',
        'Garlic Sauce Swirl',
        'Bell Peppers',
        'Red Onion'
    ],
    cheeseburger: [
        'Pickles',
        'Burgersauce',
        'Minced Meat',
        'Cheese',
        'Red Onion',
        'Fresh Tomato'
    ],
    ham: [
        'Ham',
        'Honey Mustardsauce',
        'Cheese'
    ],
    americanSupremeMeatlover: [
        'Bacon',
        'Minced Meat',
        'Cheese',
        'Chicken Kebab',
        'Pepperoni',
        'Red Onion'
    ],
    spicyChickenMeatlover: [
        'Caramelized Onion',
        'Smoked Chicken',
        'Cheese',
        'Chicken Kebab',
        'Chicken',
        'Bell Peppers',
        'Jalapenos'
    ],
    pulledBeefMeatlover: [
        'Bacon',
        'Caramelized Onion',
        'Ham',
        'Hickory BBQ Sauce Swirl',
        'Cheese',
        'Spring Onion',
        'Pulled Beef'
    ],
    bbqMixedGrill: [
        'Bacon',
        'Minced Meat',
        'Ham',
        'Cheese',
        'Chicken',
        'Bell Peppers',
        'Red Onion'
    ],
    chickenKebab: [
        'Cheese',
        'Chicken Kebab',
        'Garlic Sauce Swirl',
        'Red Onion'
    ],
    shoarma: [
        'Cheese',
        'Garlic Sauce Swirl',
        'Shoarma'
    ],
    extravaganzza: [
        'Mushrooms',
        'Minced Meat',
        'Ham',
        'Cheese',
        'Olives',
        'Bell Peppers',
        'Pepperoni',
        'Red Onion'
    ],
    fourCheese: [
        'Emmentaler',
        'Goat Cheese',
        'Gorgonzola',
        'Cheese'
    ],
    hawaii: [
        'Pineapple',
        'Ham',
        'Cheese'
    ],
    chickenSupreme: [
        'Cheese',
        'Chicken',
        'Bell Peppers',
        'Red Onion',
        'Fresh Tomato'
    ],
    hotAndSpicy: [
        'Cheese',
        'Chicken',
        'Bell Peppers',
        'Pepperoni',
        'Red Onion',
        'Jalapenos'
    ],
    caprese: [
        'Cheese',
        'Pesto',
        'Spinach',
        'Fresh Tomato'
    ],
    tonno: [
        'Cheese',
        'Red Onion',
        'Tuna'
    ],
    veggi: [
        'Mushrooms',
        'Cheese',
        'Bell Peppers',
        'Red Onion',
        'Spinach',
        'Fresh Tomato'
    ],
    americana: [
        'Minced Meat',
        'Ham',
        'Cheese',
        'Pepperoni'
    ],
    creamyBacon: [
        'Bacon',
        'Mushrooms',
        'Ham',
        'Cheese',
        'Red Onion'
    ],
    deluxe: [
        'Bacon',
        'Mushroom',
        'Minced Meat',
        'Cheese',
        'Bell Peppers',
        'Pepperoni',
        'Red Onion'
    ],
    perfectPepperoni: [
        'Cheese',
        'Pepperoni'
    ],
    margaritha: [
        'Oregano Herbs',
        'Cheese'
    ],
    funghi: [
        'Mushrooms',
        'Cheese'
    ],
    bbqBacon: [
        'Bacon',
        'Cheese',
        'Red Onion'
    ],
    twoCheeses: [
        'Emmentaler',
        'Cheese',
        'Fresh Tomato'
    ],
    salami: [
        'Cheese',
        'Salami'
    ],
    veganMargaritha: [
        'Oregano Herbs',
        'Vegan Cheese'
    ],
    veganSpicy: [
        'Bell Peppers',
        'Red Onion',
        'Jalapenos',
        'Spinach',
        'Vegan Cheese',
        'Fresh Tomato'
    ],
    veganBbq: [
        'Caramelized Onion',
        'Mushrooms',
        'Hickory BBQ Sauce Swirl',
        'Spring Onions',
        'Vegan Cheese',
        'Fresh Tomato'
    ],
    custom: [
        'Cheese'
    ],
};
//# sourceMappingURL=pizzas.js.map