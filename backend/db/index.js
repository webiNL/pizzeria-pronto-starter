"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var pizzas_1 = require("./pizzas");
var ingredients_1 = require("./ingredients");
function randomStock() {
    return Math.floor(Math.random() * 10) + 1;
}
function getPizzaTitle(input) {
    var result = input.replace(/([A-Z])/g, " $1");
    return "Pizza " + result.charAt(0).toUpperCase() + result.slice(1);
}
function getPizzaIngredients(input) {
    var ids = [];
    input.forEach(function (item) {
        var found = exports.ingredients.find(function (ingredient) { return ingredient.title === item; });
        if (found)
            ids.push(found.id);
    });
    return ids;
}
function getIngredients() {
    return ingredients_1.ingredientTitles.map(function (title, i) {
        return {
            id: i + 1,
            title: title,
            stock: randomStock(),
        };
    });
}
function getPizzas() {
    var keys = Object.keys(pizzas_1.pizzaList);
    return keys.map(function (title, i) {
        return {
            id: i + 1,
            title: getPizzaTitle(title),
            ingredients: getPizzaIngredients(pizzas_1.pizzaList[title])
        };
    });
}
exports.orders = [];
exports.ingredients = getIngredients();
exports.pizzas = getPizzas();
//# sourceMappingURL=index.js.map