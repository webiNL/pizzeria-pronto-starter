"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var middleware_1 = require("./middleware");
var controllers_1 = require("./controllers");
var app = express();
var port = Number(process.env.PORT) || 3005;
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(middleware_1.stripId);
app.all('*', middleware_1.allowCORS);
// Public endpoints for information
app.use('/', controllers_1.PublicController);
app.use('/assets', controllers_1.AssetsController);
// Actual API
app.use('/ingredients', controllers_1.IngredientsController);
app.use('/order', controllers_1.OrderController);
app.use('/pizza', controllers_1.PizzaController);
// Fire up!
app.listen(port, function () {
    console.log("\n\nBackend is listening at http://localhost:" + port + "/\nOpen in your browser for more information.\n\n");
});
//# sourceMappingURL=server.js.map