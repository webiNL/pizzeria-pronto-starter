"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function stripId(req, Res, next) {
    if (req.body.id)
        delete req.body.id;
    next();
}
exports.stripId = stripId;
function allowCORS(req, res, next) {
    // Setup CORS headers
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    // intercept OPTIONS method
    if (req.method === 'OPTIONS') {
        res.send(200);
    }
    else {
        next();
    }
}
exports.allowCORS = allowCORS;
//# sourceMappingURL=index.js.map