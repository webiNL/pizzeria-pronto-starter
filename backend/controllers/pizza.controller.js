"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var index_1 = require("../db/index");
var router = express_1.Router();
router.get('/', function (req, res) {
    var output = [];
    index_1.pizzas.forEach(function (pizza) {
        var x = pizza.ingredients.some(function (ingredient) {
            var i = index_1.ingredients.find(function (i) { return i.id === ingredient; });
            return (!i || i.stock === 0);
        });
        if (!x)
            output.push(pizza);
    });
    res.status(200).json(output);
});
exports.PizzaController = router;
//# sourceMappingURL=pizza.controller.js.map