"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var path_1 = require("path");
var router = express_1.Router();
router.get('/:file', function (req, res) {
    console.log('file request');
    res.sendFile(path_1.join(__dirname, "../public/assets/" + req.params.file));
});
exports.AssetsController = router;
//# sourceMappingURL=assets.controller.js.map