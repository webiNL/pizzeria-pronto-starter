"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var index_1 = require("./../db/index");
var router = express_1.Router();
router.get('/', function (req, res) {
    var output = [];
    index_1.ingredients.forEach(function (ingredient) {
        if (ingredient.stock !== 0)
            output.push(ingredient);
    });
    res.status(200).json(output);
});
exports.IngredientsController = router;
//# sourceMappingURL=ingredients.controller.js.map