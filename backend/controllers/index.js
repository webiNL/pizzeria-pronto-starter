"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./assets.controller"));
__export(require("./ingredients.controller"));
__export(require("./order.controller"));
__export(require("./pizza.controller"));
__export(require("./public.controller"));
//# sourceMappingURL=index.js.map