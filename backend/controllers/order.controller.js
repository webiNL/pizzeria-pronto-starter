"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var index_1 = require("../db/index");
var router = express_1.Router();
function checkAddress(input) {
    return (!!input.city && typeof input.city === 'string'
        && !!input.houseNumber && typeof input.houseNumber === 'number'
        && !!input.name && typeof input.name === 'string'
        && !!input.phoneNumber && typeof input.phoneNumber === 'string'
        && !!input.street && typeof input.street === 'string'
        && !!input.zipcode && typeof input.zipcode === 'string'
        && (input.houseNumberExt ? typeof input.houseNumberExt === 'string' : true));
}
function checkPizza(input) {
    var allowedPizzaSizes = ['14cm', '25cm', '35cm'];
    var allowedCrustTypes = ['regular', 'garlic', 'cheesy'];
    input.some(function (pizza, index) {
        if (!!pizza.ingredients && Array.isArray(pizza.ingredients)
            && !!pizza.crust && typeof pizza.crust === 'string'
            && allowedCrustTypes.indexOf(pizza.crust) !== -1
            && !!pizza.size && typeof pizza.size === 'string'
            && allowedPizzaSizes.indexOf(pizza.size) !== -1) {
            pizza.ingredients.some(function (ingredient, i) {
                var found = index_1.ingredients.find(function (item) { return item.id === ingredient; });
                if (found) {
                    if (found.stock !== 0) {
                        return false;
                    }
                    else {
                        throw new Error("Ingredient " + i + " on pizza " + index + " is out of stock");
                    }
                }
                else {
                    throw new Error("Ingredient " + i + " on pizza " + index + " does not exist");
                }
            });
            return false;
        }
        else {
            throw new Error("Pizza " + index + " was malformed");
        }
    });
}
function reserveIngredients(input) {
    input.forEach(function (pizza) {
        pizza.ingredients.forEach(function (ingredient) {
            var i = index_1.ingredients.findIndex(function (item) { return item.id === ingredient; });
            index_1.ingredients[i].stock = index_1.ingredients[i].stock === 0 ? 0 : index_1.ingredients[i].stock - 1;
        });
    });
}
router.post('/', function (req, res) {
    try {
        if (!req.body.order) {
            throw new Error('No order key present');
        }
        if (!req.body.address) {
            throw new Error('No address key present');
        }
        if (!checkAddress(req.body.address)) {
            throw new Error('Address malformed');
        }
        checkPizza(req.body.order);
        reserveIngredients(req.body.order);
        index_1.orders.push(req.body.order);
        res.status(200).json('Order received');
    }
    catch (error) {
        res.status(400).json(error.message);
    }
});
exports.OrderController = router;
//# sourceMappingURL=order.controller.js.map