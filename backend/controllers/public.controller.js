"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var path_1 = require("path");
var router = express_1.Router();
router.get('/', function (req, res) {
    res.sendFile(path_1.join(__dirname, '../public/index.html'));
});
exports.PublicController = router;
//# sourceMappingURL=public.controller.js.map