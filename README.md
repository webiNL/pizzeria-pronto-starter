# Pizzeria Pronto
> Web Integration Frontend Assignment

## Project Brief

We've prepared an intro on the backend service that you can use.

0. Install this project in the usual way.
0. Run `npm run start:backend`
0. Visit `http://localhost:3005` in your browser.

